unit multicomparescript;

{
 Script file:

  First, specify some script-wide options:
    WIDTH=****   Width (in bytes) to compare at a time. Maximum is 8, default (if not specified) is 1.
    ALIGN=****   Alignment (in bytes) to compare at. Default is equal to WIDTH, but other values ARE accepted!
    SIGNED       If present, the values are interpreted as signed (with INCREASING / DECREASING / SAME_DIRECTION comparisons).

  Start a group of files with something in [] brackets. The actual text is ignored altogether,
  use it for your own reference.

  Inside a group, specify:
    COMPARISON=*******
      SAME_ALL - Value is the same in all files.
      DIFFERENT_ALL - Value is different in every file
      DIFFERENT_FIRST - Value is different in the first file than in any other file (but may be the same in eg. the 2nd and 3rd file)
      EXACT - Value is equal to a specified value in all files
      NOT - Value is NOT equal to a specified value in all files
      GREATER - Value is greater than a specified value in all files
      LESS - Value is less than a specified value in all files
      INCREASING - Value increases from one file to the next
      DECREASING - Value decreases from one file to the next
      SAME_DIRECTION - Value increases or decreases from one file to the next, in the same direction between files

      The relative comparisons all have equivalents with "_OR_EQUAL" appended. It should be self-explanatory what these versions do.
    VALUE=****
      For EXACT or NOT comparisons, specifies the value to compare against.
    FILE=****
      Name of a file to compare. Multiple files may (of course) be specified per group, using multiple FILE lines.
}


interface

uses
  FGL, LazFileUtils,
  Classes, SysUtils;

type
  TDataComparison = (dcUnknown,
                     dcSameInAll, dcDifferentInAll, dcSpecificValue, dcNotSpecificValue, dcIncreasing, dcDecreasing, dcSameDirection,
                     dcIncreasingOrEqual, dcDecreasingOrEqual, dcSameDirectionOrEqual, dcGreater, dcLess, dcGreaterOrEqual, dcLessOrEqual,
                     dcDifferentInFirst);

  { TMultiCompareGroup }

  TMultiCompareGroup = class
    private
      fFiles: TStringList;
      fComparison: TDataComparison;
      fComparisonParam: UInt64;
    public
      constructor Create;
      destructor Destroy; override;

      function LoadFromStringList(aStringList: TStringList; aStartLine: Integer): Integer; // returns the index of the last *used* line
      procedure Clear;

      property Files: TStringList read fFiles;
      property Comparison: TDataComparison read fComparison write fComparison;
      property ComparisonParam: UInt64 read fComparisonParam write fComparisonParam;
  end;

  TMultiCompareGroups = specialize TFPGObjectList<TMultiCompareGroup>;

  { TMultiCompareScript }

  TOffsetArray = array of UInt64;

  TMultiCompareScript = class
    private
      fBasePath: String;
      fGroups: TMultiCompareGroups;
      fValueWidth: Integer;
      fAlignment: Integer;
      fSigned: Boolean;
      function GetLocalAlignment: Integer;
    public
      constructor Create;
      destructor Destroy; override;

      procedure LoadFromFile(aFilename: String);
      procedure LoadFromStringList(aStringList: TStringList);
      procedure Clear;

      function Execute: TOffsetArray;

      property BasePath: String read fBasePath write fBasePath;
      property Groups: TMultiCompareGroups read fGroups;
      property ValueWidth: Integer read fValueWidth write fValueWidth;
      property Alignment: Integer read fAlignment write fAlignment;
      property LocalAlignment: Integer read GetLocalAlignment;
      property Signed: Boolean read fSigned write fSigned;
  end;

implementation

{ TMultiCompareScript }

function TMultiCompareScript.GetLocalAlignment: Integer;
begin
  if fAlignment >= 1 then
    Result := fAlignment
  else
    Result := fValueWidth;
end;

constructor TMultiCompareScript.Create;
begin
  inherited;
  fGroups := TMultiCompareGroups.Create;
end;

destructor TMultiCompareScript.Destroy;
begin
  fGroups.Free;
  inherited Destroy;
end;

procedure TMultiCompareScript.LoadFromFile(aFilename: String);
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SL.LoadFromFile(aFilename);
    LoadFromStringList(SL);
  finally
    SL.Free;
  end;

  fBasePath := ExtractFilePath(ExpandFileNameUTF8(aFilename));
end;

procedure TMultiCompareScript.LoadFromStringList(aStringList: TStringList);
var
  i: Integer;
  ThisLine: String;
  ThisKey, ThisValue, ThisValueUnmodified: String;

  NewGroup: TMultiCompareGroup;
begin
  Clear;

  i := 0;
  while i < aStringList.Count do
  begin
    try
      if Trim(aStringList[i]) = '' then Continue;

      ThisLine := aStringList[i];
      if ThisLine[1] = '[' then
      begin
        NewGroup := TMultiCompareGroup.Create;
        Groups.Add(NewGroup);
        i := NewGroup.LoadFromStringList(aStringList, i + 1);
      end else begin
        ThisKey := Trim(Uppercase(aStringList.Names[i]));
        ThisValueUnmodified := aStringList.ValueFromIndex[i];
        ThisValue := Trim(Uppercase(ThisValueUnmodified));

        if ThisKey = 'WIDTH' then fValueWidth := StrToIntDef(ThisValue, 1);
        if ThisKey = 'ALIGN' then fAlignment := StrToIntDef(ThisValue, -1);
        if ThisKey = 'SIGNED' then fSigned := true;
      end;
    finally
      Inc(i);
    end;
  end;
end;

procedure TMultiCompareScript.Clear;
begin
  fBasePath := '';
  fGroups.Clear;
  fValueWidth := 1;
  fAlignment := 1;
  fSigned := false;
end;

function TMultiCompareScript.Execute: TOffsetArray;
var
  FinalPosition: UInt64;
  FileCount: Integer;
  Failed: array of Boolean;

  AndMask, XorMask: UInt64;

  procedure PrepareExecution;
  var
    i, i2: Integer;
    SearchRec: TSearchRec;
  begin
    AndMask := $FFFFFFFFFFFFFFFF shr ((8 - fValueWidth) * 8);
    if fSigned then
      XorMask := $8000000000000000 shr ((8 - fValueWidth) * 8)
    else
      XorMask := 0;

    FileCount := 0;
    FinalPosition := High(FinalPosition);
    for i := 0 to Groups.Count-1 do
      for i2 := 0 to Groups[i].Files.Count-1 do
      begin
        Inc(FileCount);
        if FindFirst(BasePath + Groups[i].Files[i2], faAnyFile, SearchRec) = 0 then
        begin
          try
            if UInt64(SearchRec.Size) < FinalPosition then FinalPosition := SearchRec.Size;
          finally
            FindClose(SearchRec)
          end;
        end;
      end;

    FinalPosition := FinalPosition - fValueWidth;
    FinalPosition := FinalPosition - (FinalPosition mod LocalAlignment);
    SetLength(Failed, FinalPosition div LocalAlignment);
    FillChar(Failed[0], Length(Failed) * SizeOf(Failed[0]), 0);
  end;

var
  Values: array of array of UInt64;

  procedure GetFileData(aFilename: String; aIndex: Integer);
  var
    MS: TMemoryStream;
    i: Integer;
    Cmp: UInt64;
  begin
    if not FilenameIsAbsolute(aFilename) then
      aFilename := IncludeTrailingPathDelimiter(BasePath) + aFilename;
    WriteLn('  Loading ', aFileName);
    MS := TMemoryStream.Create;
    try
      MS.LoadFromFile(aFilename);
      i := 0;
      while i <= (MS.Size - LocalAlignment) do
      begin
        try
          if MS.Position <> i then
            MS.Position := i;

          Cmp := 0;
          MS.Read(Cmp, fValueWidth);
          Cmp := (Cmp and AndMask) xor XorMask;

          Values[aIndex, i div LocalAlignment] := Cmp;
        finally
          Inc(i, LocalAlignment);
        end;
      end;
    finally
      MS.Free;
    end;
  end;

  procedure CompareFileData(aGroup: TMultiCompareGroup);
  var
    i: Integer;
    n, n2: Integer;
    Param: UInt64;
    LocalComparison: TDataComparison;
  begin
    WriteLn('  Comparing file data...');

    Param := (aGroup.ComparisonParam and AndMask) xor XorMask;

    for i := 0 to (FinalPosition div LocalAlignment) - 1 do
    begin
      if Failed[i] then Continue;
      LocalComparison := aGroup.Comparison;

      for n := 0 to aGroup.Files.Count-1 do
      begin
        case LocalComparison of
          dcSameInAll: if n = 0 then Continue else if Values[n][i] <> Values[n-1][i] then Failed[i] := true;
          dcDifferentInAll: if n = 0 then Continue else
                            begin
                              for n2 := 0 to n-1 do
                                if Values[n][i] = Values[n2][i] then
                                begin
                                  Failed[i] := true;
                                  Break;
                                end;
                            end;
          dcDifferentInFirst: if n = 0 then Continue else if Values[n][i] = Values[0][i] then Failed[i] := true;
          dcSpecificValue: if Values[n][i] <> Param then Failed[i] := true;
          dcNotSpecificValue: if Values[n][i] = Param then Failed[i] := true;
          dcGreater: if Values[n][i] <= Param then Failed[i] := true;
          dcLess: if Values[n][i] >= Param then Failed[i] := true;
          dcGreaterOrEqual: if Values[n][i] < Param then Failed[i] := true;
          dcLessOrEqual: if Values[n][i] > Param then Failed[i] := true;
          dcIncreasing: if n = 0 then Continue else if Values[n][i] <= Values[n-1][i] then Failed[i] := true;
          dcDecreasing: if n = 0 then Continue else if Values[n][i] >= Values[n-1][i] then Failed[i] := true;
          dcIncreasingOrEqual: if n = 0 then Continue else if Values[n][i] < Values[n-1][i] then Failed[i] := true;
          dcDecreasingOrEqual: if n = 0 then Continue else if Values[n][i] > Values[n-1][i] then Failed[i] := true;
          dcSameDirection: if n = 0 then Continue
                           else if (Values[n][i] < Values[n-1][i]) then LocalComparison := dcDecreasing
                           else if (Values[n][i] > Values[n-1][i]) then LocalComparison := dcIncreasing
                           else Failed[i] := true; // they're equal, in this case
          dcSameDirectionOrEqual: if n = 0 then Continue
                                  else if (Values[n][i] < Values[n-1][i]) then LocalComparison := dcDecreasingOrEqual
                                  else if (Values[n][i] > Values[n-1][i]) then LocalComparison := dcIncreasingOrEqual;
        end;

        if Failed[i] then Break;
      end;
    end;
  end;

  procedure RunGroup(aGroup: TMultiCompareGroup);
  var
    i: Integer;
  begin
    // GetFileData loads the data into a nice, sequential array that doesn't need
    // to know about alignments or which bytes overlap etc. Thus, we can separate
    // the loading and comparing code, which is a bit slower and more memory
    // intensive, but gives much cleaner code.

    WriteLn('Running new comparison group');
    SetLength(Values, aGroup.Files.Count, FinalPosition div LocalAlignment);
    for i := 0 to aGroup.Files.Count-1 do
      GetFileData(aGroup.Files[i], i);
    CompareFileData(aGroup);
  end;

  procedure RunComparisons;
  var
    i: Integer;
  begin
    for i := 0 to fGroups.Count-1 do
      RunGroup(fGroups[i]);
  end;

  procedure ProduceResults;
  var
    i, n: Integer;
    PosCount: Integer;
  begin
    WriteLn('Producing results');
    PosCount := 0;
    for i := 0 to Length(Failed)-1 do
      if not Failed[i] then Inc(PosCount);

    SetLength(Result, PosCount);
    n := 0;
    for i := 0 to Length(Failed)-1 do
      if not Failed[i] then
      begin
        Result[n] := i;
        Inc(n);
      end;
  end;

begin
  PrepareExecution;
  RunComparisons;
  ProduceResults;
  WriteLn('Execution complete');
end;

{ TMultiCompareGroup }

constructor TMultiCompareGroup.Create;
begin
  inherited;
  fFiles := TStringList.Create;
  Clear;
end;

destructor TMultiCompareGroup.Destroy;
begin
  fFiles.Free;
  inherited Destroy;
end;

function TMultiCompareGroup.LoadFromStringList(aStringList: TStringList;
  aStartLine: Integer): Integer;
var
  i: Integer;
  ThisLine: String;
  ThisKey, ThisValue, ThisValueUnmodified: String;
begin
  Clear;

  for i := aStartLine to aStringList.Count-1 do
  begin
    ThisLine := aStringList[i];
    if Trim(ThisLine) = '' then Continue;

    if ThisLine[1] = '[' then Break;

    Result := i;

    ThisKey := Trim(Uppercase(aStringList.Names[i]));
    ThisValueUnmodified := aStringList.ValueFromIndex[i];
    ThisValue := Trim(Uppercase(ThisValueUnmodified));

    if ThisKey = 'COMPARISON' then
    begin
      fComparison := dcUnknown;
      if ThisValue = 'SAME_ALL' then fComparison := dcSameInAll;
      if ThisValue = 'DIFFERENT_ALL' then fComparison := dcDifferentInAll;
      if ThisValue = 'DIFFERENT_FIRST' then fComparison := dcDifferentInFirst;
      if ThisValue = 'EXACT' then fComparison := dcSpecificValue;
      if ThisValue = 'NOT' then fComparison := dcNotSpecificValue;
      if ThisValue = 'INCREASING' then fComparison := dcIncreasing;
      if ThisValue = 'DECREASING' then fComparison := dcDecreasing;
      if ThisValue = 'SAME_DIRECTION' then fComparison := dcSameDirection;
      if ThisValue = 'INCREASING_OR_EQUAL' then fComparison := dcIncreasingOrEqual;
      if ThisValue = 'DECREASING_OR_EQUAL' then fComparison := dcDecreasingOrEqual;
      if ThisValue = 'SAME_DIRECTION_OR_EQUAL' then fComparison := dcSameDirectionOrEqual;
      if ThisValue = 'GREATER' then fComparison := dcGreater;
      if ThisValue = 'LESS' then fComparison := dcLess;
      if ThisValue = 'GREATER_OR_EQUAL' then fComparison := dcGreaterOrEqual;
      if ThisValue = 'LESS_OR_EQUAL' then fComparison := dcLessOrEqual;
      if fComparison = dcUnknown then
        raise Exception.Create('Unknown comparison: ' + ThisValue);
    end;

    if ThisKey = 'VALUE' then
      fComparisonParam := StrToIntDef(ThisValue, 0);

    if ThisKey = 'FILE' then
      fFiles.Add(ThisValueUnmodified);
  end;
end;

procedure TMultiCompareGroup.Clear;
begin
  fFiles.Clear;
  fComparison := default(TDataComparison);
  fComparisonParam := 0;
end;

end.

