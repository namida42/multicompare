program multicompare;

uses
  Classes, SysUtils,
  multicomparescript;

var
  Script: TMultiCompareScript;
  Results: TOffsetArray;
  SL: TStringList;
  i: Integer;

begin
  Script := TMultiCompareScript.Create;
  try
    Script.LoadFromFile(ParamStr(1));
    Results := Script.Execute;
    SL := TStringList.Create;
    try
      if Length(Results) = 0 then
        SL.Add('No results found.')
      else
        for i := 0 to Length(Results)-1 do
          SL.Add('0x' + IntToHex(Results[i * Script.LocalAlignment], 16));

      if ParamStr(2) = '' then
        SL.SaveToFile(ChangeFileExt(ParamStr(1), '-results.txt'))
      else
        SL.SaveToFile(ParamStr(2));
    finally
      SL.Free;
    end;
  finally
    Script.Free;
  end;
end.

